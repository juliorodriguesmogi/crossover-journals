package com.crossover.trial.journals.scheduler;


import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.crossover.trial.journals.Application;
import com.crossover.trial.journals.service.JournalDailyDigestServiceImpl;

@Component
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JobSchedulerTest {

	@Autowired
	private JournalDailyDigestServiceImpl  jdd;
	
	private static final String TIME_ZONE = "America/Sao_Paulo";
	@Scheduled(cron = "0 0 3 * * *", zone=TIME_ZONE)
	@Test
	public void JobScheduled() {
		jdd.SendDailyDigestEmail();
		
	}

}