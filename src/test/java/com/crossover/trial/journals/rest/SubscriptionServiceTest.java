package com.crossover.trial.journals.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import java.util.List;
import com.crossover.trial.journals.Application;
import com.crossover.trial.journals.model.Subscription;
import com.crossover.trial.journals.model.User;
import com.crossover.trial.journals.service.SubscriptionService;
import com.crossover.trial.journals.service.UserService;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SubscriptionServiceTest {

	@Autowired
	private SubscriptionService subscriptionService;

	@Autowired
	private UserService userService;

	@Test
	// Test the list of subscribed users
	public void browseSubscribedUser() {
		List<Subscription> subscription = subscriptionService.listAll();
		assertNotNull(subscription);
		assertNotEquals(0, subscription.size());
		// Assert to validate data content association
		subscription.stream().forEach(s -> assertNotNull(s.getUser()));
		subscription.stream().forEach(s -> assertEquals(s.getUser().getId(), userService.getUserByLoginName("user1").getId()));

	}

	@Test
	public void browseUnSubscribedUser() {
		List<User> listunSubscribedUsers = subscriptionService.getunSubscribedUsers();
		int counter=listunSubscribedUsers.size();
		List<Subscription> subscription = subscriptionService.listAll();
		for (User u: listunSubscribedUsers) {
			subscription.stream().forEach(s-> assertNotEquals(s.getUser(),u.getId()));	
		}

	}

}
