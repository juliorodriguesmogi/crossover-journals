package com.crossover.trial.journals.rest;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import com.crossover.trial.journals.Application;
import com.crossover.trial.journals.model.User;
import com.crossover.trial.journals.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Service
// Create new class for distinguish UserService tests as well and inhered methods for tests  
public class UserServiceTest {


	private List<User> listUser;
	
	@Autowired
	private UserService userService;
	

	@Test
	// Test method to validate the listAll 
	public void getAll() {
		this.listUser =  userService.listAll();
		assertNotNull(this.listUser);
	}

	
	@Test
	// Test method to iterate the user by ID checking;
	public void getbyId() {
		this.listUser =  userService.listAll();
		for (User u : this.listUser) {
			assertNotNull(u.getId());
		}
	}

	@Test
	public void getbyLoginName() {
		this.listUser =  userService.listAll();
		for (User u : listUser) {
			assertNotNull(u.getId());
		}
	}



	
}
