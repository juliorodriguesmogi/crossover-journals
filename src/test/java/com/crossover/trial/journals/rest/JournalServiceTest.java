package com.crossover.trial.journals.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.crossover.trial.journals.Application;
import com.crossover.trial.journals.model.Journal;
import com.crossover.trial.journals.model.Publisher;
import com.crossover.trial.journals.model.Subscription;
import com.crossover.trial.journals.model.User;
import com.crossover.trial.journals.repository.JournalRepository;
import com.crossover.trial.journals.repository.PublisherRepository;
import com.crossover.trial.journals.repository.SubscriptionRepository;
import com.crossover.trial.journals.repository.UserRepository;
import com.crossover.trial.journals.service.JournalMailerServiceImpl;
import com.crossover.trial.journals.service.JournalService;
import com.crossover.trial.journals.service.JournalServiceImpl;
import com.crossover.trial.journals.service.ServiceException;
import com.crossover.trial.journals.service.UserService;

import org.apache.log4j.Logger;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JournalServiceTest {

	private final static String NEW_JOURNAL_NAME = "New Journal ";
	private final static Logger log = Logger.getLogger(JournalServiceImpl.class);

	@Autowired
	private JournalService journalService;

	@Autowired
	private UserService userService;

	@Autowired
	private PublisherRepository publisherRepository;

	@Autowired
	private JournalRepository journalRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private SubscriptionRepository subscriptionRepository;

	@Autowired
	private JournalMailerServiceImpl journalMailerImpl;

	@Test
	// Test journals get data
	public void browseJournalUsers() {
		List<User> journalUsers = (List<User>) journalService.listAllJournalUsers();
		assertNotNull(journalUsers);
		assertEquals(3, journalUsers.size());

		assertEquals(new Long(1), journalUsers.get(0).getId());

	}

	// @Test
	// Removed unnecessary and out of context test method
	/*
	 * public void browseUnSubscribedUser() { List<Journal> journals =
	 * journalService.listAll(getUser("user2")); assertEquals(0, journals.size()); }
	 */

	@Test
	public void listPublisher() {
		User user = userService.getUserByLoginName("publisher1");
		Optional<Publisher> p = publisherRepository.findByUser(user);
		List<Journal> journals = journalService.publisherList(p.get());
		assertEquals(2, journals.size());

		assertEquals(new Long(1), journals.get(0).getId());
		assertEquals(new Long(2), journals.get(1).getId());

		assertEquals("Medicine", journals.get(0).getName());
		assertEquals("Test Journal", journals.get(1).getName());
		journals.stream().forEach(j -> assertNotNull(j.getPublishDate()));
		journals.stream().forEach(j -> assertEquals(new Long(1), j.getPublisher().getId()));

	}

	@Test(expected = ServiceException.class)
	public void publishFail() throws ServiceException {
		User user = userService.getUserByLoginName("publisher2");
		Optional<Publisher> p = publisherRepository.findByUser(user);

		Journal journal = new Journal();
		journal.setName("New Journal");

		journalService.publish(p.get(), journal, 1L);
	}

	@Test(expected = ServiceException.class)
	public void publishFail2() throws ServiceException {
		User user = userService.getUserByLoginName("publisher2");
		Optional<Publisher> p = publisherRepository.findByUser(user);

		Journal journal = new Journal();
		journal.setName("New Journal");

		journalService.publish(p.get(), journal, 150L);
	}

	@Test()
	public void publishSuccess() {
		User user = userService.getUserByLoginName("publisher2");
		Optional<Publisher> p = publisherRepository.findByUser(user);

		Journal journal = new Journal();
		StringBuilder sb = new StringBuilder();
		sb.append(NEW_JOURNAL_NAME);
		sb.append(Double.toString(Math.random()*1000));
		journal.setName(sb.toString());
		journal.setUuid("SOME_EXTERNAL_ID");
		try {
			journalService.publish(p.isPresent() ? p.get() : null, journal, 2L);
		} catch (ServiceException e) {
			fail(e.getMessage());
		}
		List<Journal> journals = journalService.listAll(userService.getUserByLoginName("user1"));
		assertEquals(1, journals.size());

		journals = journalService.publisherList(p.get());
		assertEquals(2, journals.size());
		assertEquals(new Long(3), journals.get(0).getId());
		assertEquals(new Long(4), journals.get(1).getId());
		assertEquals("Health", journals.get(0).getName());
		assertEquals(sb.toString(), journals.get(1).getName());
		journals.stream().forEach(j -> assertNotNull(j.getPublishDate()));
		journals.stream().forEach(j -> assertEquals(new Long(2), j.getPublisher().getId()));

	}

	@Test(expected = ServiceException.class)
	public void unPublishFail() {
		User user = userService.getUserByLoginName("publisher1");
		Optional<Publisher> p = publisherRepository.findByUser(user);
		journalService.unPublish(p.isPresent()?p.get():null, 4L);
	}

	@Test(expected = ServiceException.class)
	public void unPublishFail2() {
		User user = userService.getUserByLoginName("publisher2");
		Optional<Publisher> p = publisherRepository.findByUser(user);
		journalService.unPublish(p.isPresent()?p.get():null, 100L);
		
	}

	@Test
	public void unPublishSuccess() {
		User user = userService.getUserByLoginName("publisher2");
		Optional<Publisher> p = publisherRepository.findByUser(user);
		journalService.unPublish(p.isPresent() ? p.get() : null, 3L);

		List<Journal> journals = journalService.publisherList(p.get());
		assertEquals(1, journals.size());
		journals = journalService.listAll(userService.getUserByLoginName("user1"));
		assertEquals(1, journals.size());
	}

	@Test
	public void publishingDate() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		List<Journal> journalDigest = journalRepository.getPublishingByDate(df.format(date).toString());
		assertNotEquals(0, journalDigest.size());
	}

	@Test
	public void publishSendMail() {
		List<Subscription> subscriptionList = subscriptionRepository.findAll();
		String message = "Test message";
		String publishdate = LocalDate.now().toString();
		for (Subscription s : subscriptionList) {
			User u = userRepository.findById(s.getUser().getId());
			try {
				journalMailerImpl.prepareAndSend(u.getEmail(), "New Journal Publishing", message + ' ' + publishdate);
			} catch (Exception e) {
				log.error(e);
			} finally {
				log.error("E-mail successfully sent to smtp server");
			}
		}
	}

}
