package com.crossover.trial.journals.mailer;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.crossover.trial.journals.Application;
import com.crossover.trial.journals.service.JournalMailerServiceImpl;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import com.icegreen.greenmail.util.ServerSetupTest;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)

public class JournalMailerTest {
    private GreenMail smtpServer;

    @Autowired
    private JournalMailerServiceImpl mailClient;
    @Resource
    private JavaMailSenderImpl javaMailSenderImpl;

    
    @Before
    public void setUp() throws Exception {
        smtpServer = new GreenMail(ServerSetupTest.SMTP);
        smtpServer.start();
        javaMailSenderImpl.setPort(3025);
        javaMailSenderImpl.setHost("localhost");
    }
 
    @After
    public void tearDown() throws Exception {
        smtpServer.stop();
    }
    
    @Test
    public void shouldSendMail() throws Exception {
        //given
    	SimpleMailMessage notification = new SimpleMailMessage();
    	notification.setFrom("test@sender.com");
    	notification.setTo("test@receiver.com");
    	notification.setSubject("test subject");
    	notification.setText("test message");
    	javaMailSenderImpl.send(notification);
             	
        String recipient = "juliorodriguesmogi@gmail.com";
        String message = "Test message content";
        //when
        mailClient.prepareAndSend(recipient,"Test for e-mail sender", message);
        //then
        assertReceivedMessageContains(notification.getText());
    }
          
    private void assertReceivedMessageContains(String expected) throws IOException, MessagingException {
        MimeMessage[] receivedMessages = smtpServer.getReceivedMessages();
        assertEquals(2, receivedMessages.length);
        String content = (String) receivedMessages[0].getContent();
        assertTrue(content.contains(expected));
    }    
     
}
