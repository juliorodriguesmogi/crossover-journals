package com.crossover.trial.journals.service;

import com.crossover.trial.journals.model.Journal;
import com.crossover.trial.journals.model.Publisher;
import com.crossover.trial.journals.model.User;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface JournalService {

	List<Journal> listAll(User user);

	List<Journal> publisherList(Publisher publisher);

	Journal publish(Publisher publisher, Journal journal, Long categoryId);

	void unPublish(Publisher publisher, Long journalId);

	Collection<User> listAllJournalUsers();

	List<Journal> listAll(Optional<User> userByLoginName);
	

}
