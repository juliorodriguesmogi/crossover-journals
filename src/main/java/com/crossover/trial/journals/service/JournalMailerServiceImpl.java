package com.crossover.trial.journals.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class JournalMailerServiceImpl implements JournalMailerService {
	@Autowired
	private JavaMailSender mailSender;

	private final static Logger log = Logger.getLogger(JournalMailerService.class);

	public JournalMailerServiceImpl() {
		super();
	}

	public void prepareAndSend(String recipient, String subject, String message) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setFrom("juliorodriguesmogi@gmail.com");
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			messageHelper.setText(message);
		};
		try {
			this.mailSender.send(messagePreparator);
		} catch (MailException e) {
			// runtime exception; compiler will not force you to handle it
		} finally {
			log.error("E-mail sent to smtp server");
		}
	}
}
