package com.crossover.trial.journals.service;

import com.crossover.trial.journals.model.Subscription;
import com.crossover.trial.journals.model.User;

import java.util.List;
import java.util.Optional;

public interface SubscriptionService {
	// Create List method for subscription
	
	List<Subscription> listAll();

	List<Subscription> subscriptionList(Optional<User> optional);
	
	List<User> getunSubscribedUsers();

}
