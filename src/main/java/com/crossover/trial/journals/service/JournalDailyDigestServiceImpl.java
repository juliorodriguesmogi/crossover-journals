package com.crossover.trial.journals.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crossover.trial.journals.model.Journal;
import com.crossover.trial.journals.model.Publisher;
import com.crossover.trial.journals.model.User;
import com.crossover.trial.journals.repository.JournalRepository;
import com.crossover.trial.journals.repository.UserRepository;

@Service
@Transactional(readOnly = true)
public class JournalDailyDigestServiceImpl  {


	@Autowired
	private JournalMailerServiceImpl journalMailerImpl;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private JournalRepository journalRepository;

	// Send daily Digest Email
	public void SendDailyDigestEmail()  {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date date=new Date();
		List<Journal> journalDigest = journalRepository.getPublishingByDate(df.format(date).toString());
		List<User> userList = userRepository.findAll();
		StringBuilder message=new StringBuilder();
		String journalName,publishDate=null;
		for (Journal j: journalDigest) {
			journalName=j.getName().toString();
			publishDate=j.getPublishDate().toString();
			message.append(journalName);
			message.append(" ");
			message.append(publishDate);
		}
		for (User u : userList) {
			journalMailerImpl.prepareAndSend(u.getEmail(),"Journal Daily Digest", message.toString());
		}
		
	}

	

}
