package com.crossover.trial.journals.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.crossover.trial.journals.model.Category;
import com.crossover.trial.journals.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crossover.trial.journals.model.Subscription;
import com.crossover.trial.journals.model.User;
import com.crossover.trial.journals.repository.SubscriptionRepository;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

	@Autowired
	private SubscriptionRepository subscriptionRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	public SubscriptionServiceImpl(SubscriptionRepository subscriptionRepository) {
		this.subscriptionRepository = subscriptionRepository;
	}



	@Override
	public List<Subscription> listAll() {
		// Return the list of all Subscription records
		return subscriptionRepository.findAll();
	}
	@Override
	public List<Subscription> subscriptionList(Optional<User> user){
		// Return the list of subscriptions by specific User
		return subscriptionRepository.findByUser(user.isPresent()?user.get().getId():null);
	}



	@Override
	public List<User> getunSubscribedUsers() {
		// TODO Auto-generated method stub
		return subscriptionRepository.getunSubscribedUsers();
	}





}
