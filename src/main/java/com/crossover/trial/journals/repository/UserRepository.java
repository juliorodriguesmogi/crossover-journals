package com.crossover.trial.journals.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crossover.trial.journals.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByLoginName(String loginName);
    
    User findById(Long id);
    
    // create new repository method to collect all user records
    List<User> findAll();

}
