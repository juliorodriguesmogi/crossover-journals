package com.crossover.trial.journals.repository;

import java.util.List;

import javax.persistence.NamedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.crossover.trial.journals.model.Subscription;
import com.crossover.trial.journals.model.User;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {

	List<Subscription> findByUser(Long user_id);
	
	List<Subscription> findAll();
	
	List<User> getunSubscribedUsers();
}
