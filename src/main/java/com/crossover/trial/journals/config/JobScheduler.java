package com.crossover.trial.journals.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.crossover.trial.journals.service.JournalDailyDigestServiceImpl;

@Component
// Job scheduler to send a unique notification a day since job is started
public class JobScheduler {
	@Autowired
	private JournalDailyDigestServiceImpl  jdd;
	
	private static final String TIME_ZONE = "America/Sao_Paulo";
	
	@Scheduled(cron = "0 59 23 * * *", zone=TIME_ZONE)
	public void JobScheduled() {
		jdd.SendDailyDigestEmail();
		 
	}

}